# Shell Container

Basic Shell environment with some system inspection tools as a container.

## Usage

```bash
docker run --rm -it hojerst/shell
```
