FROM debian:12@sha256:e97ee92bf1e11a2de654e9f3da827d8dce32b54e0490ac83bfc65c8706568116 AS minimal

SHELL ["/bin/bash", "-o", "pipefail", "-c" ]

# install additional software
# hadolint ignore=SC2016,DL3008
RUN export DEBIAN_FRONTEND=noninteractive \
 && apt-get update \
 && apt-get install --no-install-recommends -y \
        curl \
        ca-certificates \
        git \
        zsh \
        yadm \
        sudo \
 && rm -rf /var/lib/apt/lists/* \
 && useradd -m -u 1000 -G sudo -s /bin/zsh user \
 && printf "Defaults:user env_keep+=SSH_AUTH_SOCK\nuser ALL=(ALL) NOPASSWD: ALL" >/etc/sudoers.d/passwordless-user

ENV USER=root
ENV SHELL=/bin/zsh
WORKDIR /root

RUN yadm clone https://gitlab.com/hojerst/dotfiles.git -f --bootstrap \
 && echo "unset POWERLEVEL9K_CONTEXT_{DEFAULT,SUDO}_CONTENT_EXPANSION" >~/.config/zsh/zzz-custom-p10k.zsh \
 && rm -Rf /tmp/*

CMD [ "/bin/zsh", "--login" ]
